# 生存之道!✿✿ヽ(°▽°)ノ✿
[搭建版本对应](https://juejin.cn/post/7087965150577164301)

## docker安装
### 安装mysql
```bash
docker pull mysql
```

*启动mysql容器*
```bash
docker run -p 3306:3306 --name mysql \
-v /mydata/mysql/log:/var/log/mysql \
-v /mydata/mysql/data:/var/lib/mysql \
-v /mydata/mysql/conf:/etc/mysql \
-v /mydata/mysql/mysql-files:/var/lib/mysql-files \
-e MYSQL_ROOT_PASSWORD=root \
-d mysql
```

*启动nacos容器*
```bash
docker run -d \
# 使用standalone单机模式
-e MODE=standalone \ 
# 使用mysql数据库
-e SPRING_DATASOURCE_PLATFORM=mysql \ 
# 数据库地址
-e MYSQL_SERVICE_HOST=127.0.0.1 \ 
# 数据库用户名
-e MYSQL_SERVICE_USER=root \ 
# 数据库密码
-e MYSQL_SERVICE_PASSWORD=root \ 
# 数据库名称
-e MYSQL_SERVICE_DB_NAME=sub-nacos \ 
-e TIME_ZONE='Asia/Shanghai' \
-e JVM_XMS=256m \
-e JVM_XMX=256m \
-e JVM_XMN=256m \
# 挂载目录
-v /mydata/nacos/logs:/home/nacos/logs \
-v /mydata/nacos/conf:/home/nacos/conf \
-p 8848:8848 \
--name nacos \
# 总是重启，若docker重启，nacos也会重启
--restart=always \
nacos/nacos-server
```
*启动redis容器*
```
docker run -d \
-p 6379:6379 \
-v /mydata/redis/conf/redis.conf:/etc/redis/redis.conf \
-v /mydata/redis/data:/data \
--restart=always \
--privileged=true \
--name redis redis redis-server /etc/redis/redis.conf \
--appendonly yes
```