package top.sneaker.gateway;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import top.sneaker.common.core.launch.SubApplication;
import top.sneaker.common.core.launch.constant.AppConstant;

@SpringBootApplication
@EnableDiscoveryClient
public class SubGatewayApplication {

    public static void main(String[] args) {
        SubApplication.run(AppConstant.APPLICATION_GATEWAY, SubGatewayApplication.class, args);
    }

}
