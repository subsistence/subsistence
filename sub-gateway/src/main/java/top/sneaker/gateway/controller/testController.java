package top.sneaker.gateway.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhaowx
 * @date 2022/4/22 0022
 */
@RestController
@RequestMapping("/test")
public class testController {

    @Value("${zwx.name}")
    private String name;

    @GetMapping
    public String test() {
        return "name: " + name;
    }
}
