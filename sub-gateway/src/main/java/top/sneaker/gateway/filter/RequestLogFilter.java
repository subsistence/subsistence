package top.sneaker.gateway.filter;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.TimeInterval;

import com.google.gson.Gson;
import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.http.server.reactive.ServerHttpResponseDecorator;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.InetSocketAddress;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.Map;


/**
 * @ClassName: RequestLogFilter
 * @Author: sense
 * @Description: 网管请求日志打印
 * @CreateDate: 2022/8/16 21:54
 */

@Component
public class RequestLogFilter implements GlobalFilter, Ordered {
    private static final Logger logger = LoggerFactory.getLogger(RequestLogFilter.class);

    private static final String REQ_HEAD = "\n====================请求信息Begin====================";

    private static final String REQ_SPILT = "\n====================请求信息  End====================";

    private static final String RESP_HEAD = "\n====================响应信息Begin====================";

    private static final String RESP_SPLIT = "\n====================响应信息  End====================";

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {

        //计时器
        TimeInterval timer = DateUtil.timer();

        StringBuilder reqMsg = new StringBuilder();
        StringBuilder respMsg = new StringBuilder();

        // 获取请求的各项信息
        ServerHttpRequest req = exchange.getRequest();
        InetSocketAddress address = req.getRemoteAddress();
        String method = req.getMethodValue();
        URI uri = req.getURI();
        HttpHeaders headers = req.getHeaders();

        Map queryMap = req.getQueryParams();
        String query = new Gson().toJson(queryMap);

        reqMsg.append(REQ_HEAD + "时间：" + DateUtil.now());
        reqMsg.append("\n 请求头=").append(headers);
        reqMsg.append("\n 参数信息=").append(query);
        reqMsg.append("\n 请求方式=").append(method);
        reqMsg.append("\n 请求路径=").append(uri.getPath());
        reqMsg.append(REQ_SPILT);
        logger.info(reqMsg.toString());

        //以下为响应
        ServerHttpResponse response = exchange.getResponse();
        DataBufferFactory bufferFactory = response.bufferFactory();
        respMsg.append(RESP_HEAD);
        ServerHttpResponseDecorator decoratedResponse = new ServerHttpResponseDecorator(response) {
            @Override
            public Mono<Void> writeWith(Publisher<? extends DataBuffer> body) {
                if (body instanceof Flux) {
                    Flux<? extends DataBuffer> fluxBody = (Flux<? extends DataBuffer>) body;
                    return super.writeWith(fluxBody.map(dataBuffer -> {
                        byte[] content = new byte[dataBuffer.readableByteCount()];
                        dataBuffer.read(content);
                        String responseResult = new String(content, Charset.forName("UTF-8"));
                        respMsg.append("\n 请求路径=").append(uri.getPath());
                        respMsg.append("\n 响应状态码=").append(this.getStatusCode());
                        respMsg.append("\n 响应头信息=").append(this.getHeaders());
                        respMsg.append("\n 响应结果=").append(responseResult);
                        respMsg.append(RESP_SPLIT);
                        respMsg.append("耗时ms:").append(timer.intervalRestart());
                        logger.info(respMsg.toString());
                        return bufferFactory.wrap(content);
                    }));
                }
                return super.writeWith(body);
            }
        };

        return chain.filter(exchange.mutate().response(decoratedResponse).build());
    }

    @Override
    public int getOrder() {
        return -10;
    }

}
