package top.sneaker.auth.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

/**
 * @Author: dhx
 * @DateL 2022/4/25 14:20
 * @ClassName: TestController
 * @Description: TODO
 * @return
 **/
@RestController
@RequestMapping
public class TestController {

    @GetMapping("/test")
    public Object testMethod(){
        return new ArrayList<String>();
    }
}
