package top.sneaker.auth;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import top.sneaker.common.core.launch.SubApplication;
import top.sneaker.common.core.launch.constant.AppConstant;

@SpringBootApplication
@EnableDiscoveryClient
public class SubAuthApplication {

    public static void main(String[] args) {
        SubApplication.run(AppConstant.APPLICATION_AUTH, SubAuthApplication.class, args);
    }

}
