package top.sneaker.system;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import top.sneaker.common.core.launch.SubApplication;
import top.sneaker.common.core.launch.constant.AppConstant;

@SpringBootApplication
public class SubSystemApplication {

    public static void main(String[] args) {
        SubApplication.run(AppConstant.APPLICATION_SYSTEM, SubSystemApplication.class, args);
    }

}
