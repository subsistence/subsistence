package top.sneaker.common.core.launch;

import cn.hutool.json.JSONUtil;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.util.StringUtils;
import top.sneaker.common.core.launch.constant.NacosConstant;

import java.util.Properties;

/**
 * 扩展启动参数
 *
 * @author zhaowx
 * @date 2022/4/22 0022
 */
public class LauncherClient {

    public void launcher(SpringApplicationBuilder builder, String appName, String profile) {
        Properties props = System.getProperties();

        String groupName = "SUB_GROUP";

        System.out.println(JSONUtil.toJsonStr(props));

        // 获取服务通用配置
        setProperty(props, "spring.cloud.nacos.config.ext-config[0].data-id", NacosConstant.extDataId(profile));
        setProperty(props, "spring.cloud.nacos.config.ext-config[0].group", groupName);
        setProperty(props, "spring.cloud.nacos.config.ext-config[0].refresh", "true");

        setProperty(props, "spring.cloud.nacos.config.ext-config[1].data-id", "sub.yaml");
        setProperty(props, "spring.cloud.nacos.config.ext-config[1].group", groupName);
        setProperty(props, "spring.cloud.nacos.config.ext-config[1].refresh", "true");


        setProperty(props, "spring.cloud.nacos.discovery.group", groupName);
        setProperty(props, "spring.cloud.nacos.config.group", groupName);

        builder.properties(props);
    }

    public static void setProperty(Properties props, String key, String value) {
        if (!StringUtils.hasLength(props.getProperty(key))) {
            props.setProperty(key, value);
        }

    }
}
