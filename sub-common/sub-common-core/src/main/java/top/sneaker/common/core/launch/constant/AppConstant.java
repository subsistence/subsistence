package top.sneaker.common.core.launch.constant;

/**
 * @author zhaowx
 * @date 2022/4/22 0022
 */
public interface AppConstant {

    String APPLICATION_GATEWAY = "sub-gateway";

    String APPLICATION_AUTH = "sub-auth";

    String APPLICATION_SYSTEM = "sub-system";
}
