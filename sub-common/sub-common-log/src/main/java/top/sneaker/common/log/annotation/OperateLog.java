package top.sneaker.common.log.annotation;


import java.lang.annotation.*;

/**
 * 请求日志打印
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface OperateLog {
    /**
     * 日志描述信息
     */
    String description() default "";
}
